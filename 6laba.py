import math

rnd = True
dict = {'G': [], 'F': [], 'Y': []}

while rnd:
    try:
        a = float(input('Введите а: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_min = float(input('Введите минимальное значение x: '))
        n = int(input('Введите количество шагов вычисления функции:'))
    except ValueError:
        print('Введенное значение - не число.')
        exit(1)

    x_lst = []

    # Вычисление шага изменения аргумента x
    try:
        step = (x_max - x_min) / n
    except ZeroDivisionError:
        print("Ошибка! Попробуйте еще раз.")

    # Блок расчета G, F, Y

    # Вычисление функции G
    for n in range(n):
        x = x_min + step * n
        x_lst.append(x)
        try:
            G = 4 * (-10 * a ** 2 + 7 * a * x + 6 * x ** 2) / (-9 * a ** 2 + 10 * a * x + 16 * x ** 2)
            dict['G'].append(G)
        except ZeroDivisionError:
            print("Ошибка!На ноль делить нельзя.")
            G = None
            dict['G'].append(G)
    # Вычисление функции F
    for n in range(n):
        x = x_min + step * n
        x_lst.append(x)
        try:
            F = math.cos(25 * a ** 2 - 50 * a * x + 9 * x ** 2)
            dict['F'].append(F)
        except ValueError:
            print("Ошибка!")
            F = None
            dict['F'].append(F)
    # Вычисление функции Y
    for n in range(n):
        x = x_min + step * n
        x_lst.append(x)
        try:
            Y = math.acosh(21 * a ** 2 + 59 * a * x + 8 * x ** 2 + 1)
            dict['Y'].append(Y)
        except ValueError:
            print("Ошибка! acosh -1<= x >= 1")
            Y = None
            dict['Y'].append(Y)

#Вывод результатов
    print(f'x: {", ".join([str(x) for x in x_lst])}')
    print(f'G: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in dict["G"]])}')
    print(f'F: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in dict["F"]])}')
    print(f'Y: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.5f}""") for y in dict["Y"]])}')

    # Продолжение/завершение цикла
    print('Если хотите продолжить нажмите цифру 1, иначе - нажмите любую другую.')

    if int(input()) != 1:
        rnd = False
    else:
        x_lst = []
        dict["G"] = []
        dict["F"] = []
        dict["Y"] = []
