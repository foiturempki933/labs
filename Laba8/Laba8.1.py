import random
import time
import lab

stop = 1000000 # Промежуток
step = 100000
n = 100000
search = 0

try:
    search = int(input('Введите радисус:'))
except ValueError:
    print('Ошибка!')
    exit(1)

file = open('time.txt', 'w')  # Создание файла

while n <= stop:
    x = [random.uniform(-50, 50) for point in range(n)]  # Создаем случайных чисел по оси x
    y = [random.uniform(-50, 50) for point in range(n)]  # По оси y
    z = (random.uniform(-50, 50), random.uniform(-50, 50))  # По оси z

    starttime = time.time()  # Время начала алгоритма
    print(f'Количество точек: {lab.counter_points(search, x, y, z)}') # Вывод Количества точек
    stoptime = time.time()  # Время конца алгоритма
    amounttime = str(stoptime - starttime).replace('.', ',')  # Количество времени, потраченного на алгоритм
    file.write(f'{amounttime}\n')  # Запись количества времени, потраченного на алгоритм в файл
    n += step
file.close() # Закрываем файл



