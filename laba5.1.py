try:
    s = int(input('Введите целое число: '))
except:
    print('0')
    exit(1)

kol = 0

if s == 0:
    count = 1

while s != 0:
    if s % 2 == 0:
        kol += 1
    s = s // 10
print(kol)

