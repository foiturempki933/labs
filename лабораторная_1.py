import math
# Блок ввода данных программы
x = float(input("Введите x: "))
a = float(input("Введите a: "))
fcn_num = int(input('Выберите функцию [1/2/3]: '))

# Блок выбора и расчета G, F, Y
if fcn_num == 1:
    try:
        G = 4 * (-10 * a ** 2 + 7 * a * x + 6 * x ** 2) / (-9 * a ** 2 + 10 * a * x + 16 * x ** 2)
        print('G = {0:.3f}' .format(G))
    except ZeroDivisionError:
        print("ZeroDivisionError")
elif fcn_num == 2:
    try:
        F = math.cos(25*a**2-50*a*x+9*x**2)
        print('F = {0:.3f}' .format(F))
    except ValueError:
        print("ValueError")
elif fcn_num == 3:
    try:
        Y = math.acosh(21*a**2+59*a*x+8*x**2+1)
        print('Y = {0:.3f}' .format(Y))
    except ValueError:
        print("ValueError")


