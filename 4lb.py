import math
import matplotlib.pyplot as plt


def y_max(y):
    y = list(filter(None, y))
    f_max = y[0]
    for i in range(len(y)):
        if y[i] > f_max:
            f_max = y[i]
    return f_max


def y_min(y):
    y = list(filter(None, y))
    f_min = y[0]
    for i in range(len(y)):
        if y[i] < f_min:
            f_min = y[i]
    return f_min


rnd = True

while rnd:

    try:
        a = float(input('Введите а: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_min = float(input('Введите минимальное значение x: '))
        fcn_num = int(input('Выберите функцию [1- G /2-F/ 3- Y]: '))
        n = int(input('Введите количество шагов вычисления функции:'))
    except ValueError:
        print('Введенное значение - не число.')
        exit(1)

    x_lst = []
    y_lst = []

    # Вычисление шага изменения аргумента x
    try:
        step = (x_max - x_min) / n
    except ZeroDivisionError:
        print("Ошибка! Попробуйте еще раз.")

    # Блок выбора функций и расчета G, F, Y
    if fcn_num != 1 and fcn_num != 2 and fcn_num != 3:
        print("Вы ввели неверный номер функции. Попробуйте еще раз.")
    # Вычисление функции 1-G
    if fcn_num == 1:
        for n in range(n):
            x = x_min + step * n
            x_lst.append(x)
            try:
                G = 4 * (-10 * a ** 2 + 7 * a * x + 6 * x ** 2) / (-9 * a ** 2 + 10 * a * x + 16 * x ** 2)
                y_lst.append(G)
                print(f'x:{x}   G: {G:.3f}')
            except ZeroDivisionError:
                print("Ошибка!На ноль делить нельзя.")
                G = None
                y_lst.append(G)
        print('Максимальное значение функции:', y_max(y_lst))
        print('Минимальное значение функции:', y_min(y_lst))

        # Построение графика
        plt.title("График функции G")  # название
        plt.xlabel("x")  # ось x
        plt.ylabel("G")  # ось y
        plt.plot(x_lst, y_lst, 'g-', label='G')  # построение графика
        plt.legend()
        plt.show()

    # Вычисление функции F

    elif fcn_num == 2:
        for n in range(n):
            x = x_min + step * n
            x_lst.append(x)
            try:
                F = math.cos(25 * a ** 2 - 50 * a * x + 9 * x ** 2)
                y_lst.append(F)
                print(f'x:{x}   F: {F:.3f}')
            except ValueError:
                print("Ошибка!")
                F = None
                y_lst.append(F)
        print('Максимальное значение функции:', y_max(y_lst))
        print('Минимальное значение функции:', y_min(y_lst))

        # Построение графика
        plt.title("График функции F")  # название
        plt.xlabel("x")  # ось x
        plt.ylabel("F")  # ось y
        plt.plot(x_lst, y_lst, 'g-', label='F')  # построение графика
        plt.legend()
        plt.show()

    # Вычисление функции Y

    elif fcn_num == 3:
        for n in range(n):
            x = x_min + step * n
            x_lst.append(x)
            try:
                Y = math.acosh(21 * a ** 2 + 59 * a * x + 8 * x ** 2 + 1)
                y_lst.append(Y)
                print(f'x:{x}   Y: {Y:.3f}')
            except ValueError:
                print("Ошибка! acosh -1<= x >= 1")
                Y = None
                y_lst.append(Y)
        print('Максимальное значение функции:', y_max(y_lst))
        print('Минимальное значение функции:', y_min(y_lst))

        # Построение графика
        plt.title("График функции Y")  # название
        plt.xlabel("x")  # ось x
        plt.ylabel("Y")  # ось y
        plt.plot(x_lst, y_lst, 'g-', label='Y')  # построение графика
        plt.legend()
        plt.show()
    else:
        print('Ошибка!')

    # Продолжение/завершение цикла

    print('Если хотите продолжить нажмите цифру 1, иначе - нажмите любую другую.')

    if int(input()) != 1:
        rnd = False
